//
//  AppConstant.swift
//
//  Created by Akshay on 24/06/19.
//  Copyright © 2016. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import CoreData

let APPDELEGATE: AppDelegate = UIApplication.shared.delegate as! AppDelegate
let isSaveData = "isSaveData"

let EntityAlbumList = "AlbumList"
let EntityFavouriteList = "FavouriteList"


let imageURl1 = "https://upload.wikimedia.org/wikipedia/en/2/2b/Green_Day_-_American_Idiot_cover.jpg"
let imageURl2 = "https://upload.wikimedia.org/wikipedia/en/8/83/TheCoast_ST2006.jpg"
let imageURl3 = "https://upload.wikimedia.org/wikipedia/en/3/33/C.J._%26_Company_Devil%27s_Gun_album.jpg"
let imageURl4 = "https://direct.rhapsody.com/imageserver/images/Alb.286996116/500x500.jpg"
let imageURl5 = "https://upload.wikimedia.org/wikipedia/en/thumb/e/e1/Sundays-blind.jpg/220px-Sundays-blind.jpg"
let imageURl6 = "https://upload.wikimedia.org/wikipedia/en/7/7e/Benatrop.jpg"
let imageURl7 = "https://upload.wikimedia.org/wikipedia/en/8/8a/Theresistance.jpg"
let imageURl8 = "https://upload.wikimedia.org/wikipedia/en/8/8a/Coldplay_-_Ghost_Stories.png"
let imageURl9 = "https://upload.wikimedia.org/wikipedia/en/4/46/One_More_Light_Remix.jpg"

let imageURl10 = "https://upload.wikimedia.org/wikipedia/en/a/aa/Rick_Ross_Mastermind.jpg"
let imageURl11 = "https://upload.wikimedia.org/wikipedia/en/thumb/3/35/The_Eminem_Show.jpg/220px-The_Eminem_Show.jpg"
let imageURl12 = "https://upload.wikimedia.org/wikipedia/en/d/d1/Think_tank_album_cover.jpg"
let imageURl13 = "https://upload.wikimedia.org/wikipedia/en/6/64/MeteoraLP.jpg"
let imageURl14 = "https://upload.wikimedia.org/wikipedia/en/2/2e/COTN_album_cover.jpg"
let imageURl15 = "https://upload.wikimedia.org/wikipedia/en/7/7a/Minutes_to_Midnight_cover.jpg"

//MARK:- SET DATA USERDEFAULTS
func userDefault(value:AnyObject, key:String){
    UserDefaults.standard.set(value, forKey: key as String )
    UserDefaults.standard.synchronize()
}
func userDefaultValueForKey(key:String?)->AnyObject?{
    return UserDefaults.standard.value(forKey: key!) as AnyObject?
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

func printLog(data:Any){
//    print(data)
}
class AppConstant: NSObject {

    
}
