//
//  ViewModelAlbums.swift
//  Assignment
//
//  Created by Apple on 25/06/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit

class ViewModelAlbums: NSObject {

    let songName : String?
    let artistName : String?
    let albumURL : String?
    let isFavourite : Bool?
    var isPlay : Bool?
    var albumIdex : Int?
    
    init(objAlbum:AlbumListObject) {
        self.songName = objAlbum.songName
        self.artistName = objAlbum.artistName
        self.albumURL = objAlbum.albumURL
        self.isFavourite = objAlbum.isFavourite
        self.isPlay = objAlbum.isPlay
        self.albumIdex = objAlbum.albumIndex
    }
}
