//
//  AlbumListObject.swift
//  Assignment
//
//  Created by Apple on 24/06/19.
//  Copyright © 2019  . All rights reserved.
//

import UIKit

class AlbumListObject: NSObject {
    
    public var songName : String?
    public var artistName : String?
    public var albumURL : String?
    public var isFavourite : Bool?
    public var isPlay : Bool?
    public var albumIndex : Int?
    
    required public init?(dictionary: NSDictionary) {
        self.songName = dictionary["songTitle"] as? String ?? ""
        self.artistName = dictionary["artistName"] as? String ?? ""
        self.albumURL = dictionary["albumURL"] as? String ?? ""
        self.isFavourite = dictionary["isFavourite"] as? Bool ?? false
        self.albumIndex = dictionary["albumIndex"] as? Int ?? 0
    }
}
