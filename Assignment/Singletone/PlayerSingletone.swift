//
//  PlayerSingletone.swift
//  MusicZed
//
//  Created by Apple on 24/06/19.
//  Copyright © 2019  . All rights reserved.
//

import UIKit
import AVFoundation

class PlayerSingletone {

    var player = AVPlayer()
    var playerLayer = AVPlayerLayer()
    
    class var shared:PlayerSingletone
    {
        struct singleton_player
        {
            static let instance_new = PlayerSingletone()
        }
        return singleton_player.instance_new
    }
    
    
    func playMusic(){
        self.player.play()
    }
    
    func pauseMusic(){
        self.player.pause()
    }
    
    func getMusicCurrentTime() -> CMTime{
        return self.player.currentTime()
    }
    
    func resetTime(){
        
        if let currentItem = PlayerSingletone.shared.player.currentItem{
            currentItem.seek(to: CMTime.zero, completionHandler: nil)
            PlayerSingletone.shared.player.pause()
        }
    }
}
