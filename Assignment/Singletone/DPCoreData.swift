//
//  DPCoreData.swift
//  Assignment
//
//  Created by Apple on 24/06/19.
//  Copyright © 2019  . All rights reserved.
//

import Foundation
import CoreData

class DPCoreData: NSObject {
    
    class var shared:DPCoreData
    {
        struct singleton_data
        {
            static let instance_new = DPCoreData()
        }
        return singleton_data.instance_new
    }

    
    //MARK:- SAVE ALBUM DATA
    func saveAlbumData(arrAlbum:NSMutableArray){
        
        let entity = NSEntityDescription.entity(forEntityName: EntityAlbumList, in: context)
        for item in arrAlbum{
            
            let objAddAlbum = NSManagedObject(entity: entity!, insertInto: context)
            if let dict = item as? NSDictionary{
                
                objAddAlbum.setValue(dict["albumName"], forKey: "songTitle")
                objAddAlbum.setValue(dict["artistName"], forKey: "artistName")
                objAddAlbum.setValue(dict["albumURL"], forKey: "albumURL")
                objAddAlbum.setValue(false, forKey: "isFavourite")
                do{
                    try context.save()
                    userDefault(value: true as AnyObject, key: isSaveData)
                }catch{
                    printLog(data: "Error...")
                }
            }
            
        }
    }
    
    //MARK:- SAVE FAVOURITE DATA
    func saveFavouriteListData(objAlbum:ViewModelAlbums,index:Int){
        
        let entity = NSEntityDescription.entity(forEntityName: EntityFavouriteList, in: context)
        let objAddAlbum = NSManagedObject(entity: entity!, insertInto: context)
        objAddAlbum.setValue(objAlbum.songName ?? "", forKey: "songTitle")
        objAddAlbum.setValue(objAlbum.albumURL ?? "", forKey: "albumURL")
        objAddAlbum.setValue(index, forKey: "albumIndex")
        
        do{
            try context.save()
        }catch{
            printLog(data: "Error...")
        }
    }
    
    //MARK:- FETCH DATA
    func fetchData(predict:NSPredicate?,entityName:String) -> [NSManagedObject]?{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        
        if predict != nil {
            
            fetchRequest.predicate = predict
        }
        do{
            let arrUsers = try context.fetch(fetchRequest)
            return arrUsers as? [NSManagedObject]
            
        }catch let error as NSError{
            print ( "Could not get data \(error), \(error.userInfo)")
            print ("Failed to fetch data\(error.localizedDescription)")
        }
        return nil
    }
}
