//
//  ViewController.swift
//  Assignment
//
//  Created by Apple on 24/06/19.
//  Copyright © 2019  . All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

let context = APPDELEGATE.persistentContainer.viewContext

class ViewController: UIViewController,UISearchBarDelegate,AlbumDelegate{
    
  
    //MARK:- PROPERTY
    @IBOutlet weak var tblAlbumList: UITableView!
    @IBOutlet weak var collectionViewFavourite: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var dataSource :TableviewDatasource<AlbumListCell,ViewModelAlbums>!
    private var collectionViewDataSource :CollectionviewDataSource<FavouriteCell,ViewModelAlbums>!
    
    private var arrAlbumObj = [ViewModelAlbums]()
    private var arrMainAlbumObj = [ViewModelAlbums]()
    private var arrFavouriteList = [ViewModelAlbums]()
    
    private var lastSelectedIndex : Int = 0
    
    //MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.initializeUI()
    }
    
    func initializeUI(){
    
        self.searchBar.delegate = self
        
        guard let _ = (userDefaultValueForKey(key: isSaveData) as? Bool) else{
            let arrAlbumList = self.setupAlbumData()
            self.addAblumData(arrAlbum: arrAlbumList)
            self.fetchAlbums()
            self.fetchFavouriteAlbums()
            return;
        }

        self.fetchAlbums() //Fetch all album list
        self.fetchFavouriteAlbums() //Fetch favourite list
    }
    
    //MARK:- UPDATE FAVOURITE DATA
    private func updateFavouriteDataSource() {
        self.collectionViewDataSource = CollectionviewDataSource(cellIdentifier: "FavouriteCell", items: self.arrFavouriteList, configureCell: { (cell, item, indexPath) in
            self.collectionViewDataSource.delegate = self
            cell.setupCellData(obj: item)
        })
        self.collectionViewFavourite.delegate = self.collectionViewDataSource
        self.collectionViewFavourite.dataSource = self.collectionViewDataSource
        self.collectionViewFavourite.reloadData()
    }
    
    //MARK:- UPDATE ALBUM DATA
    private func updateAlbumDataSource() {
        
        self.dataSource = TableviewDatasource(cellIdentifier: "AlbumListCell", items: self.arrAlbumObj) { cell, item,indexPath  in
            self.dataSource.delegate = self
            cell.btnPlaySong.isSelected = item.isPlay ?? false
            cell.btnPlaySong.setImage(UIImage(named: "play-icon"), for: .normal)
            cell.playSongBtn = {
                
                if(self.arrAlbumObj[indexPath.row].isPlay == true){
                    
                    self.arrAlbumObj[indexPath.row].isPlay = false
                    PlayerSingletone.shared.pauseMusic()
                    
                }else{
                    
                    self.arrAlbumObj[self.lastSelectedIndex].isPlay = false
                    self.arrAlbumObj[indexPath.row].isPlay = true
                    if let url = Bundle.main.url(forResource: "SoundHelix-Song", withExtension: "mp3"){
                        self.playMusicByURL(url: url)
                    }
                    
                }
                cell.btnPlaySong.isSelected = self.arrAlbumObj[indexPath.row].isPlay ?? false
                self.lastSelectedIndex = indexPath.row
                self.tblAlbumList.reloadData()
            }
            cell.setupCellData(obj: item)
        }
        
        self.tblAlbumList.dataSource = self.dataSource
        self.tblAlbumList.delegate = self.dataSource
        self.tblAlbumList.tableFooterView = UIView()
        self.tblAlbumList.reloadData()
    }
    
    
    //MARK:- SEARCHBAR DELEGATE METHODS
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.serachSongByArtistNameOrSongTitle(withText: searchText) //Search album by Artist Name or Song title
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    //MARK:- CUSTOM DELEGATE METHODS
    func addAlbumToFavourite(obj: Any, index: Int) {
        if let albumObj = obj as? ViewModelAlbums{
            addAlbumIntoFavourite(objAlbum: albumObj, index: index)
        }
    }
    
    func moveToAlbumIndex(obj: Any) {
        if let albumIndex = obj as? Int{
            let indexPath = IndexPath(row: albumIndex, section: 0)
            self.tblAlbumList.scrollToRow(at: indexPath, at: .middle, animated: true)
        }
    }
    
    //MARK:- PLAY MUSIC
    func playMusicByURL(url:URL){
        
        PlayerSingletone.shared.resetTime()
        let playerItem = AVPlayerItem(url: url)
        PlayerSingletone.shared.player = AVPlayer(playerItem: playerItem)
        PlayerSingletone.shared.playerLayer.player = PlayerSingletone.shared.player
        
        PlayerSingletone.shared.player.play()
    }
    
    //MARK:- FILTER DATA
    func serachSongByArtistNameOrSongTitle(withText searchText:String){
        
        self.arrAlbumObj = self.arrMainAlbumObj
        let filteredArray = self.arrAlbumObj.filter() { ($0.songName)!.contains(searchText) || ($0.artistName)!.contains(searchText) }
        printLog(data: filteredArray)
        
        
        if(searchText != ""){
            self.arrAlbumObj = filteredArray
        }else{
            self.arrAlbumObj = self.arrMainAlbumObj
        }
        self.updateAlbumDataSource()
    }
    
    
    //MARK:- INSERT DATA INTO CORE DATA MODEL
    func addAblumData(arrAlbum:NSMutableArray){
        DPCoreData.shared.saveAlbumData(arrAlbum: arrAlbum)
    }
    
    func addAlbumIntoFavourite(objAlbum:ViewModelAlbums,index:Int){
        
        let filteredArray = self.arrFavouriteList.filter() {($0.songName == objAlbum.songName)}
        if(filteredArray.count > 0){
            return;
        }
        DPCoreData.shared.saveFavouriteListData(objAlbum: objAlbum, index: index)
        self.fetchFavouriteAlbums()
    }
    
    //MARK:- FETCH DATA FROM CORE DATA
    func fetchAlbums(){
        
        let result = DPCoreData.shared.fetchData(predict: nil, entityName: EntityAlbumList)
        if let res = result{
            
            for item in res {
                
                let dict = NSMutableDictionary()
                dict.setValue(item.value(forKey: "songTitle") as? String, forKey: "songTitle")
                dict.setValue(item.value(forKey: "artistName") as? String, forKey: "artistName")
                dict.setValue(item.value(forKey: "albumURL") as? String, forKey: "albumURL")
                dict.setValue(item.value(forKey: "isFavourite") as? Bool, forKey: "isFavourite")
            
                let objMusic = AlbumListObject(dictionary: NSDictionary(dictionary: dict))
                arrAlbumObj.append(objMusic.map({return ViewModelAlbums(objAlbum: $0)})!)
            }
            self.arrMainAlbumObj = self.arrAlbumObj
            self.updateAlbumDataSource()
        }
     }
    
    func fetchFavouriteAlbums(){
        
        self.arrFavouriteList.removeAll()
        let result = DPCoreData.shared.fetchData(predict: nil, entityName: EntityFavouriteList)
        
        for item in result!{
            
            let dict = NSMutableDictionary()
            dict.setValue(item.value(forKey: "songTitle") as? String, forKey: "songTitle")
            dict.setValue(item.value(forKey: "albumURL") as? String, forKey: "albumURL")
            dict.setValue(item.value(forKey: "albumIndex"), forKey: "albumIndex")
            
            let objMusic = AlbumListObject(dictionary: NSDictionary(dictionary: dict))
            arrFavouriteList.append(objMusic.map({return ViewModelAlbums(objAlbum: $0)})!)
        }
        
        if(self.arrFavouriteList.count > 0){
            self.collectionViewHeightConstant.constant = 100.0
        }else{
            self.collectionViewHeightConstant.constant = 0.0
        }
        self.collectionViewFavourite.layoutIfNeeded()
        self.updateFavouriteDataSource()
       
    }
    
    //MARK:- INITIALIZE DATA
    func setupAlbumData() -> NSMutableArray{
        
        let arrAlbum = NSMutableArray()
        
        let dict = ["albumName":"American Idiot","artistName":"Green Day","albumURL":imageURl1]
        arrAlbum.add(dict)
        
        let dict1 = ["albumName":"This Is Our Lives! EP","artistName":"The Rabble","albumURL":imageURl2]
        arrAlbum.add(dict1)
        
        let dict2 = ["albumName":"Devil's Gun","artistName":"C. J. & Company","albumURL":imageURl3]
        arrAlbum.add(dict2)
        
        let dict3 = ["albumName":"Blued and Broken by Strong Fingers","artistName":"Young Cardinals","albumURL":imageURl4]
        arrAlbum.add(dict3)
        
        let dict4 = ["albumName":"Blind","artistName":"The Sundays","albumURL":imageURl5]
        arrAlbum.add(dict4)
        
        let dict5 = ["albumName":"Live from Earth","artistName":"Pat Benatar","albumURL":imageURl6]
        arrAlbum.add(dict5)
        
        let dict6 = ["albumName":"Unleashed","artistName":"Skillet","albumURL":imageURl7]
        arrAlbum.add(dict6)
        
        let dict7 = ["albumName":"Ghost Stories","artistName":"Coldplay","albumURL":imageURl8]
        arrAlbum.add(dict7)
      
        let dict8 = ["albumName":"One More Light","artistName":"Linkin Park","albumURL":imageURl9]
        arrAlbum.add(dict8)
        
        let dict9 = ["albumName":"Mastermind","artistName":"Rick Ross","albumURL":imageURl10]
        arrAlbum.add(dict9)
        
        let dict10 = ["albumName":"Recovery","artistName":"Eminem","albumURL":imageURl11]
        arrAlbum.add(dict10)
        
        let dict11 = ["albumName":"Think Tank","artistName":"Blur","albumURL":imageURl12]
        arrAlbum.add(dict11)
        
        let dict12 = ["albumName":"Meteora","artistName":"Linkin Park","albumURL":imageURl13]
        arrAlbum.add(dict12)
        
        let dict13 = ["albumName":"Bill & Ted's Bogus Journey","artistName":"Kiss","albumURL":imageURl14]
        arrAlbum.add(dict13)
        
        let dict14 = ["albumName":"Minutes to Midnight","artistName":"Linkin Park","albumURL":imageURl15]
        arrAlbum.add(dict14)
        
        return arrAlbum
    }
}
