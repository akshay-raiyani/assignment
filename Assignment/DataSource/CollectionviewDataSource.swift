//
//  CollectionViewDataSource.swift
//  Assignment
//
//  Created by Apple on 25/06/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit

class CollectionviewDataSource<Cell:UICollectionViewCell,ViewModel> : NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    //MARK:- PROPERTY
    private var cellIdentifier :String!
    private var items :[ViewModelAlbums]!
    var configureCell :(Cell,ViewModelAlbums,IndexPath) -> ()
    weak var delegate : AlbumDelegate? = nil
    
    //MARK:- INIT
    init(cellIdentifier :String, items :[ViewModelAlbums], configureCell: @escaping (Cell,ViewModelAlbums,IndexPath) -> ()) {
        self.cellIdentifier = cellIdentifier
        self.items = items
        self.configureCell = configureCell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:self.cellIdentifier,for:indexPath) as! Cell
        let item = self.items[indexPath.row]
        self.configureCell(cell,item,indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(self.delegate != nil){
            self.delegate?.moveToAlbumIndex!(obj: self.items[indexPath.row].albumIdex)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let gap : CGFloat = 5.0
        let cellWidth = (ScreenSize.SCREEN_WIDTH - (gap * 2)) / 3
        printLog(data:"Height: \(cellWidth)")

//        self.collectionViewHeightConstant.constant = cellWidth
//        self.collectionViewFavourite.layoutIfNeeded()
        return CGSize(width: (cellWidth - 20.0), height: cellWidth)
    }

}
