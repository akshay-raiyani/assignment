//
//  TableviewDatasource.swift
//  Assignment
//
//  Created by Apple on 25/06/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
@objc protocol AlbumDelegate : class {
    @objc optional func addAlbumToFavourite(obj:Any,index:Int)
    @objc optional func moveToAlbumIndex(obj:Any)
}
class TableviewDatasource<Cell :UITableViewCell,ViewModel> : NSObject, UITableViewDataSource,UITableViewDelegate {
    
    
    //MARK:- PROPERTY
    private var cellIdentifier :String!
    private var items :[ViewModelAlbums]!
    
    weak var delegate : AlbumDelegate? = nil
    
    var configureCell :(Cell,ViewModelAlbums,IndexPath) -> ()
    
    //MARK:- INIT
    init(cellIdentifier :String, items :[ViewModelAlbums], configureCell: @escaping (Cell,ViewModelAlbums,IndexPath) -> ()) {
        self.cellIdentifier = cellIdentifier
        self.items = items
        self.configureCell = configureCell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! Cell
        let item = self.items[indexPath.row]
        self.configureCell(cell,item,indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(delegate != nil){
            delegate?.addAlbumToFavourite!(obj: self.items[indexPath.row], index: indexPath.row)
        }
    }
}
