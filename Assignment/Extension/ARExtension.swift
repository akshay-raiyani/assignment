//
//  ARExtension.swift
//  BachelorBasket
//
//  Created by Apple on 24/06/19.
//  Copyright © 2018  . All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

let imageCache = NSCache<NSString, UIImage>()

extension AVPlayer {
    
    var isPlaying: Bool {
        if (self.rate != 0 && self.error == nil) {
            return true
        } else {
            return false
        }
    }
    
}

extension UIImageView{
    
    func cacheImage(urlString: String){
        let url = URL(string: urlString)
        
        image = nil
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url!) {
            data, response, error in
            if data != nil {
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data!)
                    imageCache.setObject(imageToCache!, forKey: urlString as NSString)
                    self.image = imageToCache
                    self.contentMode = UIView.ContentMode.scaleAspectFill
                    self.clipsToBounds = true
                }
            }
        }.resume()
    }
}

extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        return arrayOrdered
    }
}

