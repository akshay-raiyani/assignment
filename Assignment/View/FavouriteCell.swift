//
//  FavouriteCell.swift
//  Assignment
//
//  Created by Apple on 24/06/19.
//  Copyright © 2019  . All rights reserved.
//

import UIKit

class FavouriteCell: UICollectionViewCell {
    
    @IBOutlet weak var imgviewAlbum: UIImageView!
    @IBOutlet weak var lblSongTitle: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func setupCellData(obj:ViewModelAlbums){
        self.imgviewAlbum.cacheImage(urlString: obj.albumURL ?? "")
        self.lblSongTitle.text = obj.songName ?? ""
    }
}
