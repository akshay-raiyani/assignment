//
//  AlbumListCell.swift
//  Assignment
//
//  Created by Apple on 24/06/19.
//  Copyright © 2019  . All rights reserved.
//

import UIKit

class AlbumListCell: UITableViewCell {

    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var imgviewAlbum: UIImageView!
    @IBOutlet weak var btnPlaySong: UIButton!
    
    var playSongBtn : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func playSongAction(_ sender: UIButton) {
        
        if let button = self.playSongBtn{
            button()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCellData(obj:ViewModelAlbums){
        
        self.lblArtistName.text = obj.artistName ?? ""
        self.lblSongName.text = obj.songName ?? ""
        self.imgviewAlbum.cacheImage(urlString: obj.albumURL ?? "")
    }
}
